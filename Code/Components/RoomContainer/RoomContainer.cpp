#include "RoomContainer.h"

namespace
{
	static void RegisterRoomContainerComponent(Schematyc::IEnvRegistrar& r)
	{
		Schematyc::CEnvRegistrationScope s = r.Scope(IEntity::GetEntityScopeGUID());
		{
			Schematyc::CEnvRegistrationScope sCs = s.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CRoomContainer));
		}
	}
	CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterRoomContainerComponent);
}


void CRoomContainer::Initialize()
{
	gEnv->pThreadManager->GetThreadConfigManager()->LoadConfig("config/game.thread_config");

	IEntityTriggerComponent* pTriggerComponent = m_pEntity->CreateComponent<IEntityTriggerComponent>();
	const Vec3 boxSize = Vec3(2, 2, 2);
	const AABB bounds = AABB(boxSize * -.5f, boxSize * .5f);
	pTriggerComponent->SetTriggerBounds(bounds);
}

void CRoomContainer::ProcessEvent(const SEntityEvent& e)
{
	if (e.event == EEntityEvent::EntityEnteredThisArea)
	{
		const EntityId enteringEntity = static_cast<EntityId>(e.nParam[0]);
		IEntity* e = gEnv->pEntitySystem->GetEntity(enteringEntity);
		if (CRoomActor* c = e->GetComponent<CRoomActor>())
		{
			CryLog("Entity with ID %x entered the area", enteringEntity);
			m_enteredEntityComponent = c;
			m_enteredActorStatus = c->GetActorStatus();
			CheckTemperature(m_enteredActorStatus);
		}
	}
	else if (e.event == EEntityEvent::EntityLeftThisArea)
	{
		const EntityId leavingEntity = static_cast<EntityId>(e.nParam[0]);
		// Shouldn't need to stop thread here
	}
	else if (e.event == EEntityEvent::Update)
	{
	}
}

Cry::Entity::EventFlags CRoomContainer::GetEventMask() const
{
	return EEntityEvent::EntityEnteredThisArea | EEntityEvent::EntityLeftThisArea;
}

// Can use member variable
void CRoomContainer::CheckTemperature(CRoomActor::SActorStatus as)
{
	CryLog("Checking temp...");
	//float roomTemperature = m_atmo.m_climate.m_temp;
	//float minActorTemp = as.m_minTemp;
	//float maxActorTemp = as.m_maxTemp;

	// Test thread stopping
	// THIS CRASHES
	if (m_pDecayThread != nullptr)
	{
		CryLog("thread is non-null");
		m_pDecayThread->StopWork();
		gEnv->pThreadManager->JoinThread(m_pDecayThread, eJM_Join);
		//delete m_pDecayThread;
		//m_pDecayThread = nullptr;
	}

	STempDecayThread* pThr = new STempDecayThread(this, .0f);
	if (!gEnv->pThreadManager->SpawnThread(pThr, "TempDecayThread"))
	{
		delete pThr;
		CryFatalError("Fatal error spawning thread");
	}
	m_pDecayThread = pThr;
}
