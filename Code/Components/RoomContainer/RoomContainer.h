#pragma once

#include "StdAfx.h"

#include <thread>
#include <chrono>

#include <CrySchematyc/CoreAPI.h>
#include <CryCore/StaticInstanceList.h>

#include <CryEntitySystem/IEntityComponent.h>

#include <CryThreading/IThreadManager.h>
#include <CryThreading/CryThread.h>
#include <CryThreading/IThreadConfigManager.h>
#include <CryThreading/IJobManager.h>
#include <CryThreading/Synchronization.h>

#include "../RoomActor/RoomActor.h"
#include "../RoomAtmosphere/RoomAtmosphere.h"

class CRoomContainer final
	: public IEntityComponent
{
public:
	CRoomContainer() = default;
	virtual ~CRoomContainer() = default;

	struct STempDecayThread
		: public IThread
	{
		STempDecayThread(CRoomContainer* container, float tempToSetTo)
			: m_bShouldStop(false)
			, m_counter(0)
		{
			m_container = container;
			m_tempToSetTo = tempToSetTo;
		}

		virtual void ThreadEntry() override
		{
			float actualScale;
			if (m_container->bShouldDecayTemperature)
			{
				actualScale = .0f - m_container->m_tempChangeScale;
			}
			else
			{
				actualScale = .0f + m_container->m_tempChangeScale;
			}

			CRoomActor* actor = m_container->m_enteredEntityComponent;

			while (!m_bShouldStop)
			{
				CRoomActor::SActorStatus s = actor->GetActorStatus();
				while (s.m_actualInternalTemp != m_tempToSetTo)
				{
					s.m_actualInternalTemp += (0.01f * actualScale);
					actor->SetActorStatus(s);
					std::this_thread::sleep_for(std::chrono::seconds(5));
				}
			}
		}

		void StopWork() 
		{
			CryLog("Stopping work");
			m_bShouldStop = true;
		}

		volatile int m_counter;
		volatile bool m_bShouldStop;

		CRoomContainer* m_container;
		float m_tempToSetTo;
	};

	inline bool operator==(const CRoomContainer& r) const 
	{
		return 0 == memcmp(this, &r, sizeof(r));
	}

	inline bool operator!=(const CRoomContainer& r) const
	{
		return 0 != memcmp(this, &r, sizeof(r));
	}

	static void ReflectType(Schematyc::CTypeDesc<CRoomContainer>& d)
	{
		d.SetGUID("{515206F3-EC04-47C6-9D9B-77B52B3CCF79}"_cry_guid);
		d.SetEditorCategory("Room");
		d.SetLabel("Room Container");

		d.AddMember(&CRoomContainer::m_atmo, 'atmo', "Atmosphere", "Atmosphere", "The Atmosphere of the Room", CAtmosphereData::SAtmosphere());
		d.AddMember(&CRoomContainer::m_tempChangeScale, 'tscl', "TempChangeScale", "Temp Change Scale", "The scale at which to change temp", 0.01f);
	}

	virtual void Initialize() override;
	virtual void ProcessEvent(const SEntityEvent& e) override;
	Cry::Entity::EventFlags GetEventMask() const override;

	CAtmosphereData::SAtmosphere m_atmo;

	float m_tempChangeScale = 1.f;

private:
	void CheckTemperature(CRoomActor::SActorStatus);

	/*void UpdateTemperature();*/

	// If true, decay temperature, if false increase
	bool bShouldDecayTemperature;

	CRoomActor* m_enteredEntityComponent;
	CRoomActor::SActorStatus m_enteredActorStatus;

	STempDecayThread* m_pDecayThread = nullptr;

	bool bIsTemperatureLocked = false;
};