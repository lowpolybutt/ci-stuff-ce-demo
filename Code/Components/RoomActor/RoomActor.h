#pragma once

#include "StdAfx.h"

#include <CryEntitySystem/IEntityComponent.h>
#include <DefaultComponents/Input/InputComponent.h>
#include <DefaultComponents/Physics/RigidBodyComponent.h>
#include <DefaultComponents/Cameras/CameraComponent.h>
#include <CrySchematyc/Env/Elements/EnvComponent.h>
#include <CrySchematyc/Env/IEnvRegistrar.h>
#include <CrySchematyc/Utils/EnumFlags.h>
#include <CryCore/StaticInstanceList.h>

#include "../RoomAtmosphere/RoomAtmosphere.h"

#include "GamePlugin.h"

class CRoomActor final
	: public IEntityComponent
{
public:
	static void ReflectType(Schematyc::CTypeDesc<CRoomActor>& d)
	{
		d.SetGUID("{BD1B7556-B0B8-4ACC-9B3F-F9438A8028CE}"_cry_guid);
		d.SetLabel("Room Actor");
		d.SetEditorCategory("Room");
		d.SetDescription("Basic RoomSystem functionality");
		d.AddMember(&CRoomActor::m_actorStatus, 0, "ASS",
			"Actor Status", "Struct containing details about the Actor's status", SActorStatus());
		d.AddMember(&CRoomActor::m_moveScale, 1, "MoveScale",
			"Move Scale", "The factor to multiply movement input by", 5.f);
	}

	struct SActorStatus
	{
		inline bool operator==(const SActorStatus& r) const
		{
			return 0 == memcmp(this, &r, sizeof(r));
		}

		inline bool operator!=(const SActorStatus& r) const
		{
			return 0 != memcmp(this, &r, sizeof(r));
		}

		static void ReflectType(Schematyc::CTypeDesc<SActorStatus>& d)
		{
			d.SetGUID("{237A7CA7-23FA-42D3-A664-91971F0AEC0B}"_cry_guid);
			d.SetLabel("Actor Status");
			d.SetDescription("The Actor Status System struct");

			d.AddMember(&SActorStatus::m_baseHealth, 'base', "BaseHealth",
				"Base Health", "The base health for the actor", 100.f);
			d.AddMember(&SActorStatus::m_minTemp, 'mint', "MinTemp", "Min Temp", "The minimum temperature before internal temperature decay", 5.f);
			d.AddMember(&SActorStatus::m_maxTemp, 'maxt', "MaxTemp", "Max Temp", "The maximum temperature before internal temperature increase", 35.f);
			d.AddMember(&SActorStatus::m_baseInternalTemp, 'bint', "BInternalTemp", "Base Internal Temp", "The ideal internal temperature", 37.8f);
			d.AddMember(&SActorStatus::m_actualInternalTemp, 'aint', "AInternalTemp", "Actual Internal Temp", "The actual internal temperature", 37.8f);
		}

		float m_baseHealth;
		float m_minTemp;
		float m_maxTemp;
		float m_baseInternalTemp;
		float m_actualInternalTemp;
	};

	virtual void Initialize() override;

	SActorStatus GetActorStatus();
	void SetActorStatus(SActorStatus s);

	void SetCurrentAtmosphere(CAtmosphereData::SAtmosphere);

	float m_moveScale = 5.f;
	
	SActorStatus m_actorStatus;

	CAtmosphereData::SAtmosphere m_currentAtmosphere;

	Cry::Entity::EventFlags GetEventMask() const override;

private:
	void InitializePlayerControls();
};