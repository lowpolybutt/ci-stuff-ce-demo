#include "RoomActor.h"

namespace
{
	static void RegisterRoomActorComponent(Schematyc::IEnvRegistrar& r)
	{
		Schematyc::CEnvRegistrationScope s = r.Scope(IEntity::GetEntityScopeGUID());
		{
			Schematyc::CEnvRegistrationScope cSc = s.Register(SCHEMATYC_MAKE_ENV_COMPONENT(CRoomActor));
		}
	}

	CRY_STATIC_AUTO_REGISTER_FUNCTION(&RegisterRoomActorComponent);
}

// See link to SOCS
typedef uint64 uint46;

void CRoomActor::Initialize()
{
	m_pEntity->GetNetEntity()->BindToNetwork();
	InitializePlayerControls();
}

CRoomActor::SActorStatus CRoomActor::GetActorStatus()
{
	return m_actorStatus;
}

void CRoomActor::SetActorStatus(CRoomActor::SActorStatus s)
{
	m_actorStatus = s;
	CryLog("Setting ActorStatus: Internal Temp: %f", (float)s.m_actualInternalTemp);
}

void CRoomActor::SetCurrentAtmosphere(CAtmosphereData::SAtmosphere a)
{
	m_currentAtmosphere = a;
}

Cry::Entity::EventFlags CRoomActor::GetEventMask() const
{
	// TODO
	return Cry::Entity::EventFlags();
}

void CRoomActor::InitializePlayerControls()
{
	Cry::DefaultComponents::CInputComponent* c = m_pEntity->GetOrCreateComponent<Cry::DefaultComponents::CInputComponent>();
	/*Cry::DefaultComponents::CCameraComponent* cC = */
	m_pEntity->GetOrCreateComponent<Cry::DefaultComponents::CCameraComponent>();
	Cry::DefaultComponents::CRigidBodyComponent* rBody = m_pEntity->GetOrCreateComponent<Cry::DefaultComponents::CRigidBodyComponent>();
	c->RegisterAction("roomactor", "move_forward", [this, rBody](int activationMode, float v)
	{
		/*rBody->ApplyImpulse(Vec3(0, 1, 0) * m_moveScale);*/
		rBody->SetVelocity(Vec3(0, 1, 0) * m_moveScale);
	});

	c->RegisterAction("roomactor", "move_left", [this, rBody](int activationMode, float v)
	{
		rBody->SetVelocity(Vec3(1, 0, 0) * m_moveScale);
	});

	c->RegisterAction("roomactor", "move_back", [this, rBody](int, float)
	{
		rBody->SetVelocity(Vec3(0, -1, 0) * m_moveScale);
	});

	c->RegisterAction("roomactor", "move_right", [this, rBody](int, float)
	{
		rBody->SetVelocity(Vec3(-1, 0, 0) * m_moveScale);
	});

	c->RegisterAction("roomactor", "jump", [this, rBody](int, float)
	{
		rBody->ApplyImpulse(Vec3(0, 0, 1) * 5);
	});

	c->BindAction("roomactor", "move_forward", eAID_KeyboardMouse, eKI_W);
	c->BindAction("roomactor", "move_left", eAID_KeyboardMouse, eKI_A);
	c->BindAction("roomactor", "move_back", eAID_KeyboardMouse, eKI_S);
	c->BindAction("roomactor", "move_right", eAID_KeyboardMouse, eKI_D);
	c->BindAction("roomactor", "jump", eAID_KeyboardMouse, eKI_Space);
}
